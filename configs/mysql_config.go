package configs

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"os"
	"todoapp-server/models"
)

func InitDb()  {
	connectionString := os.Getenv("MYSQL_URI")
	db, err := sql.Open("mysql", connectionString)
	defer db.Close()
	if err != nil {
		fmt.Println(err.Error())
	}
		res, err := db.Exec(`
		CREATE TABLE IF NOT EXISTS todo (
			Id int(11) NOT NULL AUTO_INCREMENT,
			Content text NOT NULL,
		    Completed BOOLEAN DEFAULT FALSE ,
			Time datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
			PRIMARY KEY (Id)
		) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;`)
		if err != nil {
			fmt.Println(err.Error())
		}
		_, err = res.RowsAffected()
		if err != nil {
			fmt.Println(err.Error())
	}
}

func GetConnection() models.ConnectionModel{
	connectionString := os.Getenv("MYSQL_URI")
	return  models.ConnectionModel{
		DriverName: "mysql",
		DataSourceName: connectionString,
	}
}
