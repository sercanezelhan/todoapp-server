# ToDo Application 

Bu uylama; todo eklemek için golang ile geliştirilmiş vue ile bir arayüze sahip olarak geliştirilmiştir.

Uygulamaya erişim için : http://13.93.44.232:8080/

## Uygulamada Kullanılan teknolojiler

- Golang
- MySQL

## Uygulama Özellikleri

ToDo eklemek

## Kurmak için yapılanlar

Gereklilikler;

- Golang environment

`.env` dosyasının içerisine mysql bilgilerini girerek uygulamayı çalıştırabilirsiniz

Kurmak için:

```
go mod download
docker build -t sercanezelhan/todoapp-server .
docker run sercanezelhan/todoapp-server
```

## Deployment
Proje Docker Swarm çalışan bir sunucuya deploy ediliyor. Bu projedeki deploy.yml dosyası o sunucunun içinde ve aşağıdaki komutu çalıştırmak uygulamaların ayağa kalkması için yeterli:

```
docker stack deploy --compose-file docker-compose.yml todo-app
```
