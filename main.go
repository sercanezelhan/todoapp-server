package main

import (
	"fmt"
	"os"
	"time"
	"todoapp-server/configs"
	"todoapp-server/router"
)

func main() {
	fmt.Println("Project will start in 30 seconds.")
	time.Sleep(time.Second * 30)
	configs.ReadToEnvFile()
	configs.InitDb()
	e:= router.InitRouter()
	e.Logger.Fatal(e.Start(os.ExpandEnv(":18000")))
}
