package handlers

import (
	"github.com/labstack/echo/v4"
	"net/http"
	"todoapp-server/models"
	"todoapp-server/services"
)

type Handler interface {
	Create(c echo.Context) error
	GetAll(c echo.Context) error
}

type TodoHandler struct {
	service services.Service
}

func NewTodoHandler(service services.Service) Handler{
	return &TodoHandler{service}
}

func (t TodoHandler) Create(c echo.Context) error {
	var request models.Todo
	_ = c.Bind(&request)
	if len(request.Content) > 0 {
		result := t.service.Create(request)
		if len(result.Content) > 0{
			return c.JSON(http.StatusCreated, result)
		}else{
			return c.JSON(http.StatusBadRequest,"Something went wrong.")
		}
	}
	return  c.JSON(http.StatusBadRequest,"Fields cannot be left blank.")
}

func (t TodoHandler) GetAll(c echo.Context) error {
	result := t.service.GetAll()
	if len(result) > 0 {
		return c.JSON(http.StatusOK, result)
	}else{
		return c.NoContent(204)
	}
}