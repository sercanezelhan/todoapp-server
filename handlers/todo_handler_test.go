package handlers

import (
	"encoding/json"
	"github.com/golang/mock/gomock"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"
	"todoapp-server/models"
	"todoapp-server/services/mock"
)

func TestTodoHandler_Create(t *testing.T){
	controller := gomock.NewController(t)
	defer controller.Finish()
	service := mock.NewMockService(controller)

	todo:=models.Todo{
		Id: 1,
		Content: "test",
		Completed: false,
		Time: time.Now().UTC(),
	}
	service.EXPECT().Create(todo).Return(todo)
	requestAsString,_ := json.Marshal(todo)
	url := "/api/v1/todo"
	handler:=NewTodoHandler(service)
	e := echo.New()
	req,_ := http.NewRequest(http.MethodPost, url, strings.NewReader(string( requestAsString)))
	req.Header.Set("Content-Type", "application/json")
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	if assert.NoError(t,handler.Create(c)) {
		assert.Equal(t, http.StatusCreated, rec.Code)
	}
}

func TestTodoHandler_GetAll(t *testing.T){
	controller := gomock.NewController(t)
	defer controller.Finish()
	service := mock.NewMockService(controller)

	todos:=[]models.Todo{
		models.Todo{Id: 1,Content: "test0", Completed: false,Time: time.Now().UTC()},
		models.Todo{Id: 2,Content: "test1", Completed: false,Time: time.Now().UTC()},
	}
	service.EXPECT().GetAll().Return(todos).AnyTimes()
	url := "/api/v1/allTodo"
	handler:=NewTodoHandler(service)
	e := echo.New()
	req,_ := http.NewRequest(http.MethodGet, url,nil)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	if assert.NoError(t,handler.GetAll(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)
	}
}