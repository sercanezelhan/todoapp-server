package services

import (
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
	"todoapp-server/models"
	"todoapp-server/repositories/mock"
)

func TestTodoService_Create(t *testing.T){
	controller := gomock.NewController(t)
	defer controller.Finish()

	todo:=models.Todo{
		Id: 1,
		Content: "test",
		Completed: false,
		Time: time.Now().UTC(),
	}
	repository := mock.NewMockRepository(controller)
	repository.EXPECT().Create(todo).Return(todo)
	created := repository.Create(todo)
	assert.Equal(t, "test", created.Content)
	assert.False(t, created.Completed)
}

func TestTodoService_GetAll(t *testing.T){
	controller := gomock.NewController(t)
	defer controller.Finish()

	todos:=[]models.Todo{
		models.Todo{Id: 1,Content: "test0", Completed: false,Time: time.Now().UTC()},
		models.Todo{Id: 2,Content: "test1", Completed: false,Time: time.Now().UTC()},
	}
	repository := mock.NewMockRepository(controller)
	repository.EXPECT().GetALl().Return(todos)
	created := repository.GetALl()
	assert.Equal(t, 2, len(created))
}