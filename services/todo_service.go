//go:generate mockgen -source todo_service.go -destination mock/todo_service_mock.go -package mock
package services

import (
	"time"
	"todoapp-server/models"
	"todoapp-server/repositories"
)

type Service interface {
	Create(todo models.Todo) models.Todo
	GetAll()[]models.Todo
}

type TodoService struct {
	repository repositories.Repository
}

func NewTodoService(repository repositories.Repository) Service {
	return &TodoService{repository}
}

func (t TodoService) Create(todo models.Todo) models.Todo {
	todo.Completed=false
	todo.Time=time.Now().UTC()
	result:=t.repository.Create(todo)
	return result
}

func (t TodoService) GetAll() []models.Todo{
	return t.repository.GetALl()
}