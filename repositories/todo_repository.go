//go:generate mockgen -source todo_repository.go -destination mock/todo_repository_mock.go -package mock
package repositories

import (
	"database/sql"
	"fmt"
	"todoapp-server/models"

	_ "github.com/golang/mock/mockgen/model"
)

type Repository interface {
	Create(todo models.Todo) models.Todo
	GetALl()[]models.Todo
	Delete(id int64)bool
}

type TodoRepository struct {
	connection models.ConnectionModel

}
func NewTodoRepository(connection models.ConnectionModel) Repository {
	return &TodoRepository{connection}
}

func (t TodoRepository) Create(todo models.Todo) models.Todo {
    db, _ :=sql.Open(t.connection.DriverName,t.connection.DataSourceName)
	defer db.Close()
	stmt, err := db.Prepare("INSERT INTO todo (Content,COMPLETED,TIME) values (?,?,?)")
	if err!=nil{
		fmt.Sprint(err.Error())
	}
	result,err := stmt.Exec(&todo.Content,&todo.Completed,&todo.Time)
	id,_:=result.LastInsertId()
	todo.Id=id
	return todo
}

func (t TodoRepository) GetALl() []models.Todo {
	db, _ :=sql.Open(t.connection.DriverName,t.connection.DataSourceName)
	defer db.Close()
	result, err := db.Query("SELECT * FROM todo ORDER BY Time DESC")
	if err!=nil{
		fmt.Sprint(err.Error())
	}
	var todos []models.Todo = make([]models.Todo, 0)
	for result.Next() {
		var todo models.Todo
		err = result.Scan(&todo.Id, &todo.Content, &todo.Completed, &todo.Time)
		if err != nil {
			print(err.Error())
		}
		todos = append(todos, todo)
	}
	return todos
}

func (t TodoRepository) Delete(id int64) bool{
	db, _ :=sql.Open(t.connection.DriverName,t.connection.DataSourceName)
	defer db.Close()
	stmt, err := db.Prepare("DELETE FROM todo WHERE id=?")
	if err!=nil{
		fmt.Sprint(err.Error())
		return false
	}
	stmt.Exec(&id)
	return true
}