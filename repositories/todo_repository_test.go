package repositories

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
	"todoapp-server/configs"
	"todoapp-server/models"
)

func TestCouchDbRepository_Create(t *testing.T)  {
	configs.ReadToEnvFile()
	todo := models.Todo{
		Content: "test",
		Completed: false,
		Time: time.Now().UTC(),
	}
	repository := NewTodoRepository(configs.GetConnection())
	result:= repository.Create(todo)
	assert.Equal(t,"test",result.Content)
	assert.False(t,result.Completed)
	defer repository.Delete(result.Id)
}

func TestCouchDbRepository_GetAll(t *testing.T)  {
	configs.ReadToEnvFile()
	todo := models.Todo{
		Content: "test",
		Completed: false,
		Time: time.Now().UTC(),
	}
	var allResultId []int64
	repository := NewTodoRepository(configs.GetConnection())
	for i:=0;i<5;i++{
		result:= repository.Create(todo)
		allResultId = append(allResultId,result.Id)
	}
	result:=repository.GetALl()
	assert.Equal(t, result[0].Content,"test")
	assert.Equal(t, result[1].Content,"test")
	assert.Equal(t, result[2].Content,"test")
	assert.Equal(t, result[3].Content,"test")
	assert.Equal(t, result[4].Content,"test")
	for i:=0;i< len(allResultId);i++{
		defer repository.Delete(allResultId[i])
	}
}

func TestCouchDbRepository_Delete(t *testing.T)  {
	configs.ReadToEnvFile()
	todo := models.Todo{
		Content: "test",
		Completed: false,
		Time: time.Now().UTC(),
	}
	repository := NewTodoRepository(configs.GetConnection())
	id := repository.Create(todo).Id
	result:=repository.Delete(id)
	assert.True(t, result)
}