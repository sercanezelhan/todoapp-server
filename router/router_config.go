package router

import (
	"net/http"
	"todoapp-server/configs"
	"todoapp-server/handlers"
	"todoapp-server/repositories"
	"todoapp-server/services"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func InitRouter() *echo.Echo {
	repository := repositories.NewTodoRepository(configs.GetConnection())
	service := services.NewTodoService(repository)
	handler := handlers.NewTodoHandler(service)

	e := echo.New()
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowMethods: []string{http.MethodGet, http.MethodPut, http.MethodPost, http.MethodDelete},
	}))
	v1 := e.Group("/api/v1")
	{
		v1.POST("/todo", handler.Create)
		v1.GET("/allTodo", handler.GetAll)
	}
	return e
}
